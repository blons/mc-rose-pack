import os
import sys
import zipfile
import shutil
from datetime import datetime

def log(text):
    logtime = datetime.now().strftime('%M:%S.%f')
    print(f'[{logtime}] {text}')

def cls():
    if os.name == 'nt':
        _ = os.system('cls')
    else:
        _ = os.system('clear')

def twochar(number):
    if len(str(number)) == 1:
        return '0' + str(number)
    elif len(str(number)) == 2:
        return str(number)

def folder_files(dir):
    paths = []
    for root, dir, files in os.walk(dir):
        for filename in files:
            filepath = os.path.join(root, filename)
            paths.append(filepath)
    return paths        

def getpackver(mcver):
    match mcver:
        case 7| 8:
            return 1
        case 9| 10:
            return 2
        case 11| 12:
            return 3
        case 13| 14:
            return 4
        case 15:
            return 5
        case 16:
            return 6
        case 17:
            return 7
        case 18:
            return 8
        case 19:
            return 9

def gennfo():
    global VER, TYP, AGE, COL, PCK, FILE
    with open('temp/data.nfo', 'w') as nfo:
        nfo.write(f'custom_name={FILE}\n')
        nfo.write(f'game_version={VER}\n')
        nfo.write(f'true_version=1.{VER}\n')
        nfo.write(f'game_edition={TYP}\n')
        nfo.write(f'style={AGE}\n')
        nfo.write(f'colour={COL}\n')
        nfo.write(f'mcmetaver={PCK}\n')

def extract(age, col):
    match age, col:
        case 1, 1:
            with zipfile.ZipFile(resource_path('resource.files'), 'r') as res:
                res.extract('0/0', 'temp/res')
        case 1, 2:
            with zipfile.ZipFile(resource_path('resource.files'), 'r') as res:
                res.extract('0/2', 'temp/res')
        case 2, 1:
            with zipfile.ZipFile(resource_path('resource.files'), 'r') as res:
                res.extract('0/1', 'temp/res')
        case 2, 2:
            with zipfile.ZipFile(resource_path('resource.files'), 'r') as res:
                res.extract('0/3', 'temp/res')

def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")
    return os.path.join(base_path, relative_path)

VER = 18
TYP = 1
AGE = 1
COL = 1
PCK = 8
FILE = 0

def storeVER(selection):
    global VER
    selection = selection.split('.')
    selection = selection[1]
    VER = int(selection)

def storeTYP(selection):
    global TYP
    match selection:
        case 'Java':
            TYP = 1
        case 'Bedrock':
            TYP = 2

def storeAGE(selection):
    global AGE
    match selection:
        case 'Modern Default':
            AGE = 1
        case 'Programmer Art':
            AGE = 2

def storeCOL(selection):
    global COL
    match selection:
        case 'Red':
            COL = 1
        case 'Blue (PE)':
            COL = 2

def java():
    global VER, TYP, AGE, COL, PCK, FILE
    # JAVA
    # FOLDERS
    os.mkdir('temp/assets')
    os.mkdir('temp/assets/minecraft')
    os.mkdir('temp/assets/minecraft/lang')
    os.mkdir('temp/assets/minecraft/textures')
    if PCK < 4:
        os.mkdir('temp/assets/minecraft/textures/blocks')
    else:
        os.mkdir('temp/assets/minecraft/textures/block')
    log('Created temp')
    # FILES
    with open('temp/pack.mcmeta', 'w') as mcmeta:
        mcmetacontent = '{"pack": {"pack_format": ' + str(PCK) + ', "description": "Replaces poppies with roses."}}'
        mcmeta.write(mcmetacontent)
    log('Created mcmeta')
    if PCK < 4:
        with open('temp/assets/minecraft/lang/en_US.lang', 'w') as lang:
            lang.write('tile.flower2.poppy.name=Rose')
        with open('temp/assets/minecraft/lang/en_GB.lang', 'w') as lang:
            lang.write('tile.flower2.poppy.name=Rose')
    else:
        with open('temp/assets/minecraft/lang/en_us.json', 'w') as lang:
            lang.write('{"block.minecraft.poppy": "Rose", "block.minecraft.potted_poppy": "Potted Rose"}')
        with open('temp/assets/minecraft/lang/en_gb.json', 'w') as lang:
            lang.write('{"block.minecraft.poppy": "Rose", "block.minecraft.potted_poppy": "Potted Rose"}')
    log('Created lang')
    extract(AGE, COL)
    log('Extracted texture')
    resfile = os.listdir('temp/res/0/')[0]
    if PCK < 4:
        os.rename(f'temp/res/0/{resfile}', 'temp/assets/minecraft/textures/blocks/flower_rose.png')
    else:
        os.rename(f'temp/res/0/{resfile}', 'temp/assets/minecraft/textures/block/poppy.png')
    log('Moved texture')
    if AGE == 1:
        with zipfile.ZipFile(resource_path('resource.files'), 'r') as res:
            res.extract('1/0', 'temp/res')
    else:
        with zipfile.ZipFile(resource_path('resource.files'), 'r') as res:
            res.extract('1/1', 'temp/res')
    log('Extracted pack')
    resfile = os.listdir('temp/res/1/')[0]
    os.rename(f'temp/res/1/{resfile}', 'temp/pack.png')
    log('Moved pack')
    # CLEANUP1
    os.rmdir('temp/res/0')
    os.rmdir('temp/res/1')
    os.rmdir('temp/res')
    log('Cleaned up res')
    # PACKING
    time = datetime.now().strftime('%y%m%d%H%M%S')
    FILE = f'{time}-{twochar(VER)}{TYP}{AGE}{COL}.zip'
    log('Got time')
    gennfo()
    log('Created nfo')
    files = folder_files('temp/')
    with zipfile.ZipFile(f'output/{FILE}', 'w') as end:
        for item in files:
            end.write(item, item.replace('temp/', ''))
    log('Packaged file')
    # CLEANUP2
    if PCK < 4:
        os.remove('temp/assets/minecraft/textures/blocks/flower_rose.png')
        os.remove('temp/assets/minecraft/lang/en_US.lang')
        os.remove('temp/assets/minecraft/lang/en_GB.lang')
        os.rmdir('temp/assets/minecraft/textures/blocks')
    else:
        os.remove('temp/assets/minecraft/textures/block/poppy.png')
        os.remove('temp/assets/minecraft/lang/en_us.json')
        os.remove('temp/assets/minecraft/lang/en_gb.json')
        os.rmdir('temp/assets/minecraft/textures/block')
    os.remove('temp/pack.png')
    os.remove('temp/pack.mcmeta')
    os.remove('temp/data.nfo')
    os.rmdir('temp/assets/minecraft/textures')
    os.rmdir('temp/assets/minecraft/lang')
    os.rmdir('temp/assets/minecraft')
    os.rmdir('temp/assets')
    os.rmdir('temp')
    log('Cleaned temp')

def bedrock():
    global VER, TYP, AGE, COL, PCK, FILE
    # BEDROCK
    # FOLDERS
    os.mkdir('temp/texts')
    os.mkdir('temp/textures')
    os.mkdir('temp/textures/blocks')
    log('Created temp')
    # FILES
    with open('temp/manifest.json', 'w') as manifest:
        manifestcontent = '{"format_version": 2,"header": {"description": "Replaces poppies with roses.","name": "Bring back Roses","uuid":"623b8bb2-ab09-4baf-bccb-bec3407a0615","version": [1, 0, 0],"min_engine_version": [1, ' + str(VER) + ', 0]},"modules": [{"description": "Replaces poppies with roses.","type": "resources","uuid": "78a25c6f-26e1-4c62-af2a-f6f7538402df","version": [1, 0, 0]}]}'
        manifest.write(manifestcontent)
    log('Created manifest')
    with open('temp/texts/en_US.lang', 'w') as lang:
        lang.write('tile.red_flower.poppy.name=Rose\n')
        lang.write('howtoplay.dyes.text.2=Some dye materials are harder to find than others. While most Dyes can be crafted from flowers like Red Dye from a Rose, some Dyes are found or created in more obscure ways such as: ')
    with open('temp/texts/en_GB.lang', 'w') as lang:
        lang.write('tile.red_flower.poppy.name=Rose\n')
        lang.write('howtoplay.dyes.text.2=Some dye materials are harder to find than others. While most Dyes can be crafted from flowers like Red Dye from a Rose, some Dyes are found or created in more obscure ways such as: ')
    log('Created lang')
    extract(AGE, COL)
    log('Extracted texture')
    resfile = os.listdir('temp/res/0/')[0]
    os.rename(f'temp/res/0/{resfile}', 'temp/textures/blocks/flower_rose.png')
    log('Moved texture')
    if AGE == 1:
        with zipfile.ZipFile(resource_path('resource.files'), 'r') as res:
            res.extract('1/2', 'temp/res')
    else:
        with zipfile.ZipFile(resource_path('resource.files'), 'r') as res:
            res.extract('1/3', 'temp/res')
    log('Extracted pack')
    resfile = os.listdir('temp/res/1/')[0]
    os.rename(f'temp/res/1/{resfile}', 'temp/pack_icon.png')
    log('Moved pack')
    # CLEANUP1
    os.rmdir('temp/res/0')
    os.rmdir('temp/res/1')
    os.rmdir('temp/res')
    log('Cleaned up res')
    # PACKING
    time = datetime.now().strftime('%y%m%d%H%M%S')
    FILE = f'{time}-{twochar(VER)}{TYP}{AGE}{COL}.mcpack'
    log('Got time')
    gennfo()
    log('Created nfo')
    files = folder_files('temp/')
    with zipfile.ZipFile(f'output/{FILE}', 'w') as end:
        for item in files:
            end.write(item, item.replace('temp/', ''))
    log('Packaged file')
    # CLEANUP2
    os.remove('temp/textures/blocks/flower_rose.png')
    os.remove('temp/texts/en_US.lang')
    os.remove('temp/texts/en_GB.lang')
    os.remove('temp/pack_icon.png')
    os.remove('temp/manifest.json')
    os.remove('temp/data.nfo')
    os.rmdir('temp/textures/blocks')
    os.rmdir('temp/textures')
    os.rmdir('temp/texts')
    os.rmdir('temp/')
    log('Cleaned temp')

def gen():
    global VER, TYP, AGE, COL, PCK, FILE
    try:
        os.mkdir('temp')
    except:
        shutil.rmtree(r'temp')
        os.mkdir('temp')
    PCK = getpackver(VER)
    if TYP == 1:
        log('Starting gen.java()')
        java()
    elif TYP == 2:
        log('Starting gen.bedrock()')
        bedrock()
    return FILE

if __name__ == "__main__":
    input('run main.py')

log('Init gen.py')