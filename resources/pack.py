import os
import zipfile

path = os.path.dirname(os.path.realpath(__file__))

names = {
    0: ['/0/0', '/0/red_modern.png'],
    1: ['/0/1', '/0/red_classic.png'],
    2: ['/0/2', '/0/blue_modern.png'],
    3: ['/0/3', '/0/blue_classic.png'],
    4: ['/1/0', '/1/java_modern.png'],
    5: ['/1/1', '/1/java_classic.png'],
    6: ['/1/2', '/1/bugrock_modern.png'],
    7: ['/1/3', '/1/bugrock_classic.png'],
}

print()

def decrypt():
    for i in range(len(names)):
        os.rename(path + names[i][0], path + names[i][1])
    return True

def encrypt():
    for i in range(len(names)):
        os.rename(path + names[i][1], path + names[i][0])
    return True

def pack():
    ziplocale = path + '/resource.files'
    with zipfile.ZipFile(ziplocale, 'w') as res:
        for i in range(len(names)):
            res.write(path + names[i][1], names[i][0][1:])

# debug = {
#     '[decrypt]': decrypt,
#     '[encrypt]': encrypt,
#     '[pack]': pack,
# }

d = input('Pack files? ')
print(path)
if d == 'DECRYPT':
    decrypt()
elif d == 'ENCRYPT':
    encrypt()
else:
    pack()
    input('Successfully packed files')