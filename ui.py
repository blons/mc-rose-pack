import gen
import os
import tkinter
import time
import threading
from tkinter import ttk

def ui():
    def lgtext(textstr):
        lg.config(text=textstr)
    
    def waitbutton():
        time.sleep(3)
        trunner['state'] = tkinter.NORMAL
        gen.log('Unlocked button')
    
    def waitlabel():
        tcachename = lg.cget('text')
        time.sleep(8)
        if lg.cget('text') == tcachename:
            lgtext(f'Press run')
            gen.log('Changed label')
        else:
            gen.log('Label change cancelled')

        
    def main():
        trunner['state'] = tkinter.DISABLED
        gen.log('Locked button')
        lgtext(f'Created {gen.gen()}')
        w = threading.Thread(target=waitbutton)
        w.start()
        w = threading.Thread(target=waitlabel)
        w.start()
        gen.log('Started unlock threads')

    lgwintext = 'Press run'
    
    t = tkinter.Tk()
    t.title('GUI')
    style = ttk.Style()

    if os.name == 'nt':
        style.theme_use("winnative")
        gen.log('NT detected')
    else:
        style.theme_use("default")
        gen.log('Other detected')

    lf = ttk.LabelFrame(t, height=40, borderwidth=5, text='Options')
    lf.pack(fill="x", padx=5)

    f1 = ttk.Frame(lf, height=40)
    f1.pack(fill="x", pady=1)
    f2 = ttk.Frame(lf, height=40)
    f2.pack(fill="x", pady=1)
    f3 = ttk.Frame(lf, height=40)
    f3.pack(fill="x", pady=1)
    f4 = ttk.Frame(lf, height=40)
    f4.pack(fill="x", pady=1)

    l1 = ttk.Label(f1, text= 'Game version: ')
    l1.pack(side = tkinter.LEFT)
    verlist = ['1.18', '1.8', '1.9', '1.10', '1.11', '1.12', '1.13', '1.14', '1.15', '1.16', '1.17', '1.18', '1.19']
    versions = tkinter.StringVar(t)
    d1 = ttk.OptionMenu(f1, versions, *verlist, command=gen.storeVER)
    d1.config(width = 15)
    d1.pack(side = tkinter.RIGHT)

    l2 = ttk.Label(f2, text= 'Game edition: ')
    l2.pack(side = tkinter.LEFT)
    lanlist = ['Java', 'Java', 'Bedrock']
    langs = tkinter.StringVar(t)
    d2 = ttk.OptionMenu(f2, langs, *lanlist, command=gen.storeTYP)
    d2.config(width = 15)
    d2.pack(side = tkinter.RIGHT)

    l3 = ttk.Label(f3, text= 'Texture design: ')
    l3.pack(side = tkinter.LEFT)
    texlist = ['Modern Default', 'Modern Default', 'Programmer Art']
    texts = tkinter.StringVar(t)
    d3 = ttk.OptionMenu(f3, texts, *texlist, command=gen.storeAGE)
    d3.config(width = 15)
    d3.pack(side = tkinter.RIGHT)

    l4 = ttk.Label(f4, text= 'Texture colour: ')
    l4.pack(side = tkinter.LEFT)
    collist = ['Red', 'Red', 'Blue (PE)']
    cols = tkinter.StringVar(t)
    d4 = ttk.OptionMenu(f4, cols, *collist, command=gen.storeCOL)
    d4.config(width = 15)
    d4.pack(side = tkinter.RIGHT)

    trunner = ttk.Button(t, text= 'Run', command=main)
    trunner.pack(fill="x", padx=5, pady=3)

    s = ttk.Style()
    s.configure('t.TFrame', background='black')
    ft = ttk.Frame(t, height=30, style='t.TFrame')
    ft.pack(fill="x", padx=1, pady=1)
    
    s.configure('ft.TLabel', foreground='white', background='black')
    lg = ttk.Label(ft, justify=tkinter.LEFT, text=lgwintext, style='ft.TLabel')
    lg.pack(fill="x")

    gen.log('Setup ui')
    t.resizable(False, False)
    t.mainloop()
    gen.log('Closed ui')

def popup(name):
    p = tkinter.Tk()
    l = tkinter.Label(p, text= f"Successfully created {name}")
    l.pack()


if __name__ == "__main__":
    input('run main.py')

gen.log('Init ui.py')